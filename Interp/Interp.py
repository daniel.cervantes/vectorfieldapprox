
import numpy as np
from scipy import linalg
from scipy.spatial.distance import cdist, pdist, squareform

class Interp(object):
    
    def _h_inverse_multiquadric(self,r):
        return 1.0/np.sqrt((1.0/self.epsilon*r)**2+1)
    
    def _h_multiquadric(self, r):
        return np.sqrt((1.0/self.epsilon*r)**2 + 1)

    
    def _init_function(self,r):
        #self._function =  _h_inverse_multiquadric        
        a0 = self._h_multiquadric(r)
        if a0.shape != r.shape:
            raise ValueError("Callable must take array and return array of " 
            "the same shape")

        return a0

    def __init__(self, *args, **kwargs):

        self.xi   = np.asarray([np.asarray(a,dtype=np.float_).flatten() for a in args[:-1]])
        self.N    = self.xi.shape[-1]
        self.mode = kwargs.pop('mode','1-D')

        if self.mode == '1-D':
            self.di = np.asarray(args[-1]).flatten()
            self._target_dim = 1
        elif self.mode == 'N-D':
            self.di = np.asarray(args[-1])
            self._target_dim = self.di.shape[-1]
        else:
            raise ValueError("Mode has to be 1-D or N-D.")

        if not all ([x.size == self.di.shape[0] for x in self.xi]):
            raise ValueError("All arrays must be equal length.")

        self.norm    = kwargs.pop('norm','euclidean')
        self.epsilon = kwargs.pop('epsilon',None)
        
        if self.epsilon is None:
            # default epsilon is the "the average distance between nodes" based
            # on a bounding hypercube
            ximax = np.amax(self.xi, axis=1)
            ximin = np.amin(self.xi, axis=1)
            edges = ximax - ximin
            edges = edges[np.nonzero(edges)]
            self.epsilon = np.power(np.prod(edges)/self.N, 1.0/edges.size)

        
        if self._target_dim > 1:
            self.nodes = np.zeros( (self.N,self._target_dim) ,dtype=self.di.dtype)
            lu,piv = linalg.lu_factor(self.A)
            for i in range(self._target_dim):
                self.nodes[:,i] = linalg.lu_solve((lu,piv),self.di[:,i])
        else:
            self.nodes = linalg.solve(self.A,self.di)


    @property
    def A(self):
        r = squareform(pdist(self.xi.T,self.norm))
        return self._init_function(r)

    def _call_norm(self,x1,x2):
        return cdist(x1.T,x2.T,self.norm)

    def __call__(self,*args):
        args = [np.asarray(x) for x in args]
        if not all([x.shape == y.shape for x in args for y in args]):
            raise ValueError("Arrays lengths must be equal")
        if self._target_dim > 1:
            shp = args[0].shape + (self._target_dim,)
        else:
            shp = args[0].shape
            
        xa = np.asarray([a.flatten() for a in args], dtype=np.float_)
        r = self._call_norm(xa,self.xi)
        return np.dot(self._h_multiquadric(r),self.nodes).reshape(shp)
    
        
