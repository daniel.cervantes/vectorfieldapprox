
import numpy as np
import math 
from scipy import linalg
from scipy.special import xlogy
from scipy.spatial.distance import cdist, pdist, squareform

class Whabha(object):
    
    def _h_phi(self,r):
        return (1.0/16.0*math.pi)*xlogy(r**2,r)
    
    def _init_function(self,r):
        
        a0 = self._h_phi(r)
        if a0.shape != r.shape:
            raise ValueError("Callable must take array and return array of " 
            "the same shape")

        return a0

    def __init__(self, *args, **kwargs):
        self.xi   = np.asarray([ np.asarray(a,dtype=np.float_).flatten() for a in args[:-1] ])
        
        
        self.N    = self.xi.shape[-1]
        self.mode = kwargs.pop('mode','1-D')

        
        
        if self.mode == '1-D':
            self.di = np.asarray(args[-1]).flatten()
            self._target_dim = 1
        elif self.mode == 'N-D':
            self.di = np.asarray(args[-1])
            self._target_dim = self.di.shape[-1]
        else:
            raise ValueError("Mode has to be 1-D or N-D.")

        if not all ([x.size == self.di.shape[0] for x in self.xi]):
            raise ValueError("All arrays must be equal length.")


        self.norm   = kwargs.pop('norm','euclidean')
        self.smooth = kwargs.pop('smooth', 0.0)
        
        if self._target_dim > 1:
            self.nodes = np.zeros((self.N+3,self._target_dim),dtype=self.di.dtype)
            lu,piv = linalg.lu_factor(self.A)            
            for i in range(self._target_dim):
                di = self.di[:,i]
                di = np.append(di,[0,0,0])
                #self.nodes[:,i] = linalg.lu_solve((lu,piv),self.di[:,i])                
                self.nodes[:,i] = linalg.lu_solve((lu,piv),di)                
        else:
            self.nodes = linalg.solve(self.A,self.di)

    @property
    def A(self):
        r = squareform(pdist(self.xi.T,self.norm))
        G = np.zeros((self.N+3, self.N+3))

        G[0:self.N,0:self.N]            = self._init_function(r) - self.N*np.eye(self.N)*self.smooth
        G[0:self.N,self.N:self.N+1]     = np.ones((self.N,1))
        G[0:self.N,self.N+1:self.N+3]   = self.xi.T
        G[self.N:self.N+1,0:self.N]     = np.ones((1,self.N))
        G[self.N+1:self.N+3,0:self.N]   = self.xi
        return G 

    
    def _call_norm(self,x1,x2):
        return cdist(x1.T,x2.T,self.norm)

    def __call__(self,*args):

        args = [np.asarray(x) for x in args]
        
        dim    = len(args)
        ne,me  = args[0].shape

        xy     = np.zeros((ne*me,dim))
        nn     = self.xi.shape[1]        
        i = 0
        for x in args:
            xy[:,i] = np.concatenate(x)
            i = i + 1

        if not all([x.shape == y.shape for x in args for y in args]):            
            raise ValueError("Arrays lengths must be equal")

        if self._target_dim > 1:
            shp = args[0].shape + (self._target_dim,)
        else:
            shp = args[0].shape
            
        Ge = np.zeros((ne*me,nn+3)) 

        xa = np.asarray([a.flatten() for a in args], dtype=np.float_)
        r = self._call_norm(xa,self.xi)
        
        Ge[0:ne*me,0:nn]      = self._h_phi(r)
        Ge[0:ne*me,nn:nn+1]   = np.ones((ne*me,1))
        Ge[0:ne*me,nn+1:nn+3] = xy

        return np.dot(Ge,self.nodes).reshape(shp)
    
        
