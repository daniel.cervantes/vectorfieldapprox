from numpy import random, exp
import numpy as np

import sys
sys.path.append('../../Interp')
from Interp  import Interp  

x = random.rand(250, 1)*4 - 2
y = random.rand(250, 1)*4 - 2
d = x*exp(-x**2 - y**2)


rbf = Interp(x, y, d, epsilon=.01)

xx = np.linspace(-2, 2, 100)
yy = np.linspace(-2, 2, 100)

xv, yv = np.meshgrid(xx, yy)

di = rbf(xv, yv)

di.shape = xv.shape


import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm

# plt.pcolor(xv, yv, di)
# plt.colorbar()

#plt.scatter(x, y, 100, d,cmap=cm.jet)
#plt.contour(xv, yv, di, 20, cmap='RdGy');

# fig = plt.figure()
# ax = fig.add_subplot(111, projection='3d')
# ax.scatter(xv, yv, di)
# ax.set_xlabel('X Label')
# ax.set_ylabel('Y Label')
# ax.set_zlabel('Z Label')

plt.imshow(di, extent=[-.4, .4, -.4, .4], origin='lower',cmap='RdGy')
plt.colorbar()
plt.axis(aspect='image')

plt.show()
