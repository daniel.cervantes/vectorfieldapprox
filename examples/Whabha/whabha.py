#python whabha.py 
from numpy import random, exp
import numpy as np
import matplotlib.pyplot as plt
import chart_studio.plotly as py

import sys
sys.path.append('../../Whabha')
from Whabha import Whabha  

def testFunction(x,y):
    vx =  ( 27*(9*x+1)*exp(-(9*y+1)**2/10 -(9*x+1)**2/49))/98 - (27*(9*x-2)*exp(-(9*y-2)**2/4-(9*x-2)**2/4))/8 - (9*(9*x-7)*exp(-(9*y-3)**2/4-(9*x-7)**2/4))/4  + (18*(9*x-4)*exp(-(9*y-7)**2-(9*x-4)**2))/5;
    vy = ( 27*(9*y+1)*exp( -(9*y+1)**2/10 -(9*x+1)**2/49))/20 - (27*(9*y-2)*exp(-(9*y-2)**2/4-(9*x-2)**2/4))/8 - (9*(9*y-3)*exp(-(9*y-3)**2/4-(9*x-7)**2/4))/4 + (18*(9*y-7)*exp(-(9*y-7)**2-(9*x-4)**2))/5;

    return vx,vy


def f1(x,y):
    u = -2*x*exp(-y**2-x**2)
    v = -2*y*exp(-y**2-x**2)
    return u,v

def f2(x,y):
    u =  2*y*exp(-y**2-x**2)
    v = -2*x*exp(-y**2-x**2)
    return u,v

def f(x,y,c):
    uf1,vf1 = f1(x,y)
    uf2,vf2 = f2(x,y)

    u = 1.0/(1.0+c)*(uf1 + c*uf2);
    v = 1.0/(1.0+c)*(vf1 + c*vf2);
    return u,v


a = -2
b =  2
c = 1.0
# x = a + (b-a)*random.rand(10,)
# y = a + (b-a)*random.rand(10,)


x = np.linspace(a, b, 50)
y = np.linspace(a, b, 50)
xx, yy = np.meshgrid(x,y)

u,v  = f(xx,yy,c)

uft = np.asarray(u.flatten())
vft = np.asarray(v.flatten())

uv   = np.vstack([uft, vft]).T #= [u,v]

plt.quiver(xx,yy,u,v,color='blue')
#plt.show()


whabhaApprox = Whabha(xx, yy, uv, smooth=.00001,mode='N-D')

xe = np.linspace(a, b, 30)
ye = np.linspace(a, b, 30)
xxe, yye = np.meshgrid(xe, ye)

uve = whabhaApprox(xxe, yye)

ue = uve[:,:,0];
ve = uve[:,:,1];


plt.quiver(xxe,yye,ue,ve,color='red')
plt.show()



































# from numpy import random, exp
# import numpy as np
# import matplotlib.pyplot as plt

# import sys
# sys.path.append('../../Whabha')
# from Whabha import Whabha  

# def testFunction(x,y):
#     vx =  ( 27*(9*x+1)*exp(-(9*y+1)**2/10 -(9*x+1)**2/49))/98 - (27*(9*x-2)*exp(-(9*y-2)**2/4-(9*x-2)**2/4))/8 - (9*(9*x-7)*exp(-(9*y-3)**2/4-(9*x-7)**2/4))/4  + (18*(9*x-4)*exp(-(9*y-7)**2-(9*x-4)**2))/5;
#     vy = ( 27*(9*y+1)*exp( -(9*y+1)**2/10 -(9*x+1)**2/49))/20 - (27*(9*y-2)*exp(-(9*y-2)**2/4-(9*x-2)**2/4))/8 - (9*(9*y-3)*exp(-(9*y-3)**2/4-(9*x-7)**2/4))/4 + (18*(9*y-7)*exp(-(9*y-7)**2-(9*x-4)**2))/5;

#     return vx,vy


# x = random.rand(50, )*4 - 2
# y = random.rand(50, )*4 - 2
# d0 = x*exp(-x**2 - y**2)
# d1 = x*exp(-x**2 - y**2)
# d = np.vstack([d0, d1]).T

# x = np.linspace(0, 1, 10)
# y = np.linspace(0, 1, 10)
# xx, yy = np.meshgrid(x, y)

# x = random.rand(100, )
# y = random.rand(100, )

# vx,vy = testFunction(x,y)

# # print(vx,vy)
# v = np.vstack([vx, vy]).T




# # x = [7.2436e+05,1.9512e+05,3.2342e+05,4.4014e+05,5.5573e+05,7.2555e+05,1.9333e+05,3.2228e+05,4.3973e+05,5.5612e+05,7.2555e+05,1.9333e+05,3.2228e+05,4.3973e+05,5.5612e+05,7.2555e+05,1.9333e+05,3.2228e+05,4.3973e+05,5.5612e+05,7.2555e+05,1.9333e+05,3.2228e+05,4.3973e+05,5.5612e+05]

# # y=[2.4112e+06,2.4214e+06,2.4294e+06,2.4352e+06,2.4351e+06,2.3250e+06,2.3263e+06,2.3244e+06,2.3236e+06,2.3236e+06,2.2250e+06,2.2263e+06,2.2244e+06,2.2236e+06,2.2236e+06,2.1250e+06,2.1263e+06,2.1244e+06,2.1236e+06,2.1236e+06,2.0250e+06,2.0263e+06,2.0244e+06,2.0236e+06,2.0236e+06]

# # u=[-1.93924,-1.95366,-1.03956,-1.56292,-1.50000,1.36808,0.43578,-0.20927,0.20927,-1.99513,1.62784,3.07831,1.89104,1.03008,-1.65808,2.34736,2.57150,1.98509,0.42262,-1.94874,3.69397,1.48629,0.68200,0.62932,-0.65606]

# # v=[-3.49848,-4.60252,-4.89074,-3.68202,-2.59808,-3.75877,-4.98097,-2.99269,-2.99269,0.13951,-4.72759,-3.94005,-0.65114,-1.71433,1.11839,-4.41474,-1.54511,-0.24374,-0.90631,-0.44990,-4.72806,-1.33826,-0.73135,0.77715,-0.75471]


# uv = np.vstack([u, v]).T



# whabhaApprox = Whabha(x, y, uv, smooth=10,mode='N-D')

# # xe = np.linspace(0, 1, 30)
# # ye = np.linspace(0, 1, 30)
# xe = np.linspace(0,900000,50)
# ye = np.linspace(1.9e6,2.5e6,50)
# xxe, yye = np.meshgrid(xe, ye)

# uve = whabhaApprox(xxe, yye)

# plt.quiver(x,y,u,v)
# plt.quiver(xxe,yye,uve)
# plt.show()

