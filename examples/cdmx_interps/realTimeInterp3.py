#!/usr/bin/python3

#python cvfInterp.py 

import requests
import pandas as pd
import calendar
import requests
import time
import schedule
from datetime import datetime
import numpy as np
import sys
import json
#import datetime

#from netCDF4 import Dataset
import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap

from scipy import stats
import math 

import imageio
import os


sys.path.append('../../Utils')
from Utils  import ReadWindCDMXJson  
from Utils  import ReadCCDMXJson

sys.path.append('../../Whabha')
from Whabha import Whabha  

sys.path.append('../../Interp')
from Interp  import Interp  



def toCartesian(wdr,wsp):
    tmp = (360-(wdr-90))*math.pi/180
    return wsp*np.cos(tmp),wsp*np.sin(tmp)


def read_html_data(parameter,month,year):
    
    url = 'http://www.aire.cdmx.gob.mx/estadisticas-consultas/concentraciones/respuesta.php?qtipo=HORARIOS&parametro='
    url = url+parameter+'&anio='+year+'&qmes='+month
    
    tables_list = pd.read_html(url)
    table = tables_list[0][1:]

    return table.to_numpy()


def removeNRStation(table):
    nStations = table.shape[1]

    table_wsp_not_nr = []
    ii = []
    for i in range(nStations):
        if table[:,i].all() != 'nr':
            table_wsp_not_nr.append(table[:,i])
            ii.append(i)

    return np.vstack(table_wsp_not_nr).T,ii


def getInterpData(parameter,month,year,nHours):
    
    table_all  = read_html_data(parameter,month,year)
    table      = table_all[-nHours:]
    table,ii   = removeNRStation(table)

    return table_all[0][ii],np.array(table)


def manageNR(wsp,wdr,lon,lat):
    iwdr = np.array(np.where(wsp == 'nr'))
    iwsp = np.array(np.where(wdr == 'nr'))

    if not iwdr.all() and not iwsp.all():
        return wsp,wdr,lon,lat

    else:
        if iwdr.all() == iwsp.all():
            wsp = np.delete(wsp,iwdr,0)
            wdr = np.delete(wdr,iwdr,0)
            lon = np.delete(lon,iwdr,0)
            lat = np.delete(lat,iwdr,0)
            
            return wsp,wdr,lon,lat

        else:
            uiw = np.union1d(iwdr, iwsp)
            wsp = np.delete(wsp,uiw,0)
            wdr = np.delete(wdr,uiw,0)
            lon = np.delete(lon,uiw,0)
            lat = np.delete(lat,uiw,0)

            return wsp,wdr,lon,lat


def manageNRP(vparameter,lon,lat):

    ivp = np.array(np.where(vparameter == 'nr'))

    if not ivp.any(): 
        return vparameter,lon,lat
    else:
        vparameter = np.delete(vparameter,ivp,0)
        lon = np.delete(lon,ivp,0)
        lat = np.delete(lat,ivp,0)
        
        return vparameter,lon,lat



def saveGraph(cont):
    
    now       = datetime.now() 
    date_time = now.strftime("%m/%d/%Y, %H:%M:%S")
    month     = now.strftime("%m")
    year      = now.strftime("%Y")
    hour      = now.strftime("%H")
    minutes   = int(now.strftime("%M"))

    nHours = 24
        
    with open('../../data/CDMX/monitoring_stations.json') as f:
        station_dict = json.load(f)
    

    stations_wdr,table_wdr_last_nHours = getInterpData('wdr',month,year,nHours)
    stations_wsp,table_wsp_last_nHours = getInterpData('wsp',month,year,nHours)
    stations_cont,table_cont_last_nHours   = getInterpData(cont,month,year,nHours)
    
    
    wind_nCStation = stations_wsp.shape[0] - 2 
    wind_nStation  = len(station_dict)

    cont_nCStation = stations_cont.shape[0] - 2 
    cont_nStation  = len(station_dict)
    
    wind_lons_table = []
    wind_lats_table = []
    for i in range(2,wind_nCStation+2):
        cstation  = stations_wsp[i]        
        for j in range(wind_nStation): 
            if station_dict[j]["cve_estac"] == cstation:
                wind_lats_table.append(float(station_dict[j]["latitud"]))
                wind_lons_table.append(float(station_dict[j]["longitud"])) 


    cont_lons_table = []
    cont_lats_table = []
    for i in range(2,cont_nCStation+2):
        cstation  = stations_cont[i]        
        for j in range(cont_nStation): 
            if station_dict[j]["cve_estac"] == cstation:
                cont_lats_table.append(float(station_dict[j]["latitud"]))
                cont_lons_table.append(float(station_dict[j]["longitud"])) 


    #CDMX bounding box
    a = -99.402253
    b = -98.894845
    c = 19.000569
    d = 19.605717
    
    mp = Basemap(projection = 'merc',
                 llcrnrlon = a,
                 llcrnrlat = c,
                 urcrnrlon = b,
                 urcrnrlat = d,
                 resolution = 'h')

    
    wind_lats_table = np.array(wind_lats_table)
    wind_lons_table = np.array(wind_lons_table)
    cont_lats_table   = np.array(cont_lats_table)
    cont_lons_table   = np.array(cont_lons_table)

    vector = np.vectorize(np.float)
    filenames = []

    for i in range(nHours):
                
        wsp = np.array(table_wsp_last_nHours[i][2:])
        wdr = np.array(table_wdr_last_nHours[i][2:])
        cont_array  = np.array(table_cont_last_nHours[i][2:])

        
        wsp,wdr,wind_lons,wind_lats      = manageNR(wsp,wdr,wind_lons_table,wind_lats_table)
        cont_array,cont_lons,cont_lats   = manageNRP(cont_array,cont_lons_table,cont_lats_table)        

        mp_wind_lons, mp_wind_lats = mp(wind_lons,wind_lats)
        mp_cont_lons, mp_cont_lats = mp(cont_lons,cont_lats)

    
        # ########################################################################
        # WIND Approximation
        len_wsp = len(wsp)
        len_wdr = len(wdr)

        wsp_nr = np.where(wsp == 'nr')
        wdr_nr = np.where(wdr == 'nr')
        
        if(len_wsp != 0 and len_wdr != 0 and len(wsp_nr[0]) == 0 and len(wdr_nr[0]) == 0):            


            wsp = vector(wsp)
            wdr = vector(wdr)
            
            u,v = toCartesian(wdr,wsp)
            uv  = np.vstack([u,v]).T
            whabhaApprox = Whabha(mp_wind_lons,mp_wind_lats, uv, smooth=.00001,mode='N-D')
            
            vecf_coords_lon = np.arange(a,b,.03)
            vecf_coords_lat = np.arange(c,d,.03)
            vecf_x_lon, vecf_y_lat = np.meshgrid(vecf_coords_lon,vecf_coords_lat)
            vecf_xxe,vecf_yye = mp(vecf_x_lon,vecf_y_lat)
            
            uve = whabhaApprox(vecf_xxe,vecf_yye)
            ue = uve[:,:,0];
            ve = uve[:,:,1];
        
        # ########################################################################
        # Contaminant Approximation
        
        len_cont = len(cont_array)
        cont_nr  = np.where(cont_array == 'nr') 
        
        if(len_cont != 0 and len(cont_nr[0]) == 0):            
            
            cont_array   = vector(cont_array)
            rbf          = Interp(mp_cont_lons,mp_cont_lats,cont_array,epsilon=.000001)
            c_coords_lon = np.arange(a,b,.005)
            c_coords_lat = np.arange(c,d,.005)
            c_x_lon, c_y_lat = np.meshgrid(c_coords_lon,c_coords_lat)
            c_xx,c_yy = mp(c_x_lon,c_y_lat)
            
            
            c_zz     = rbf(c_xx, c_yy)
            c_scheme = mp.pcolor(c_xx,c_yy,c_zz,cmap='jet')
            cbar     = mp.colorbar(c_scheme,location='right',pad='10%')
        
        # #########################################################################
        
        plt.quiver(vecf_xxe,vecf_yye,ue,ve,color='red')        
        plt.quiver(mp_wind_lons,mp_wind_lats,u,v,color='black')
        
        mp.drawcoastlines()
        mp.drawstates()
        mp.drawcountries()
        
        mp.scatter(mp_wind_lons, mp_wind_lats, marker = 'o', color='black', zorder=3)        
        #mp.scatter(mp_cont_lons, mp_cont_lats, marker = 'o', color='brown', zorder=3)
        
        plt.title( cont+ ' Pollutant and wind velocities for : '+table_wsp_last_nHours[i][0]+ ', ' +table_wsp_last_nHours[i][1]+' hrs.' )
        

        # create file name and append it to a list
        filename = f'images/frame_{i}.png'
        filenames.append(filename)
        
        
        # last frame of each viz stays longer
        #if (i  n_frames):
        # for j in range(10):
        #     filenames.append(filename)
        

        # save frame
        figure = plt.gcf()                
        figure.set_size_inches(10, 10)        
        plt.savefig(filename)

        # plt.figure(figsize=(4,2))

        plt.close()# build gif

    
    output_file = '../../webpage/assets/'+cont+'_mygif.gif'
    with imageio.get_writer(output_file, mode='I',fps=0.2) as writer:
        for filename in filenames:
            image = imageio.imread(filename)
            writer.append_data(image)
            
    #Remove files
    for filename in set(filenames):
        os.remove(filename)

    #plt.show()


def save_graphs():
    saveGraph("o3")
    saveGraph("pm10")
    saveGraph("so2")


if __name__ == "__main__":
    #schedule.every(1).minutes.do(save_graphs)
    schedule.every().hour.do(save_graphs) # sets the function to run once per hour
    
    while True:  # loops and runs the scheduled job indefinitely 
        schedule.run_pending()
        #schedule.every().hour.do(save_graphs) # sets the function to run once per hour
        # time.sleep(1)
        # save_graphs()
    
    
