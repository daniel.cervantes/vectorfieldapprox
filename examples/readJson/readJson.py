
import numpy as np

import sys
sys.path.append('../../Utils')
from Utils  import ReadWindFieldJson  

reader = ReadWindFieldJson('../../data/Yucatan/wind_field_Yucatan.json')

x = reader._x()
y = reader._y()
u = reader._u()
v = reader._v()

print(x)
print(y)
print(u)
print(v)
