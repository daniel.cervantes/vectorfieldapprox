#!/usr/bin/python3.6
#To run:
#python bembourhim.py 50 .001 1000

#To reference check: Pseudo-polyharmonic vectorial approximation
#for div-curl and elastic semi-norms
#Mohammed-Najib Benbourhim
#Abderrahman Bouhamidi


from numpy import random, exp
import numpy as np
import matplotlib.pyplot as plt
import sys

sys.path.append('../../Bembourhim')
sys.path.append('../../Utils')

from Bembourhim import Bembourhim  
from Utils  import ReadWindFieldJson  

def f1(x,y):
    u = -2*x*exp(-y**2-x**2)
    v = -2*y*exp(-y**2-x**2)
    return u,v

def f2(x,y):
    u =  2*y*exp(-y**2-x**2)
    v = -2*x*exp(-y**2-x**2)
    return u,v

def f(x,y,c):
    uf1,vf1 = f1(x,y)
    uf2,vf2 = f2(x,y)

    u = 1.0/(1.0+c)*(uf1 + c*uf2);
    v = 1.0/(1.0+c)*(vf1 + c*vf2);
    return u,v

def main():
    
    N     = int(sys.argv[1])
    c     = float(sys.argv[2])
    rho   = float(sys.argv[3])
    
    ##Uniform scattered data
    # a = -2
    # b =  2
    # c = 150.0
    # xx = np.linspace(a, b, 20)
    # yy = np.linspace(a, b, 20)
    # x,y = np.meshgrid(xx,yy)
    # u,v  = f(x,y,c)
    
    
    #Random scattered data
    a = -2
    b =  2
    x = a + (b-a)*random.rand(N,)
    y = a + (b-a)*random.rand(N,)
    u,v  = f(x,y,c)
    uv   = [u,v]
    #plt.quiver(x,y,u,v,color='blue')
    bemApprox = Bembourhim(x, y, uv, rho=rho)

    
    #Uniform data
    x = np.linspace(a, b, 20)
    y = np.linspace(a, b, 20)
    xx, yy = np.meshgrid(x,y)
    uu,vv  = f(xx,yy,c)
    uft = np.asarray(uu.flatten())
    vft = np.asarray(vv.flatten())   
    uv  = [uft.T,vft.T]
    q = plt.quiver(x,y,uu,vv,color='blue',width=.005)
    plt.quiverkey(q,1.07,1.02, 1,label='exact')

    # Q = ax1.quiver(X, Y, U, V, units='width')
    # qk = ax1.quiverkey(Q, 0.9, 0.9, 2, r'$2 \frac{m}{s}$', labelpos='E', coordinates='figure')
    
    #Approximation
    xe = np.linspace(a,b,20)
    ye = np.linspace(a,b,20)
    xxe, yye = np.meshgrid(xe, ye)    
    ue,ve = bemApprox(xxe, yye)
    q2 = plt.quiver(xxe,yye,ue,ve,color='red',width=.005)
    plt.quiverkey(q2,1.07,0.95, 1,label='approx')

    u_approx = np.array([ue.flatten(), ve.flatten()])
    u_exact  = np.array([uu.flatten(), vv.flatten()])           
    
    error = np.linalg.norm(u_approx-u_exact,2)/np.linalg.norm(u_exact,2)
    
    #plt.title('Relative error: '+str(error))
    #plt.title(r'$f_c(x,y)=(f_1(x,y)+cf_2(x,y))/(1+c)$ \n djkls', fontsize='small')
    #ax.set_title('Normalized occupied \n Neighbors')

    plt.title('Approximation '+'\n'+r'$f_c(x,y)=(f_1(x,y)+cf_2(x,y))/(1+c)$'+ '\n'+r'$\rho$='+str(rho)+', c='+str(c)+', N='+str(N) + '\n'+ 'Relative error:'+str(error), fontsize='small')

    print("{0:6e}".format(error))
    plt.show()

    
     
if __name__ == "__main__":
    main()
        
        
