import json
import numpy as np
import datetime

class ReadWindFieldJson(object):
    
    def _x(self):
        return self.x

    def _y(self):
        return self.y

    def _u(self):
        return self.u

    def _v(self):
        return self.v


    def __init__(self, *args, **kwargs):

        self.nameJsonFile = args[0]

        with open(self.nameJsonFile) as json_file:
            data = json.load(json_file)

        self.x = np.asarray( [np.asarray(c['xy'][0]) for c in data['coords']] );
        self.y = np.asarray( [np.asarray(c['xy'][1]) for c in data['coords']] );
        self.u = np.asarray( [np.asarray(c['uv'][0]) for c in data['coords']] );
        self.v = np.asarray( [np.asarray(c['uv'][1]) for c in data['coords']] );


class ReadDataJson(object):
    
    def _x(self):
        return self.x

    def _y(self):
        return self.y

    def _z(self):
        return self.z

    def __init__(self, *args, **kwargs):

        self.nameJsonFile = args[0]

        with open(self.nameJsonFile) as json_file:
            data = json.load(json_file)

        self.x = np.asarray( [np.asarray(c['xy'][0]) for c in data['coords']] );
        self.y = np.asarray( [np.asarray(c['xy'][1]) for c in data['coords']] );
        self.u = np.asarray( [np.asarray(c['uv'][0]) for c in data['coords']] );
        self.v = np.asarray( [np.asarray(c['uv'][1]) for c in data['coords']] );


     
class ReadCCDMXJson(object):

    def _data(self):
        return self.data

    def _cvalue(self,date,contaminant,station):
        return self.data["pollutionMeasurements"]["date"][date][contaminant][station]
    
    def _getNPCbyD(self,date,contaminant,station,hrs = 24):

        a = np.empty(shape=(hrs, 1), dtype=float)
        for i in range(hrs):
            if(i == 23):                
                a[i] =  self.data["pollutionMeasurements"]["date"][date.strftime("%Y-%m-%d 24:00")][contaminant][station]
            else:
                a[i] =  self.data["pollutionMeasurements"]["date"][date.strftime("%Y-%m-%d %H:%M")][contaminant][station]

            date += datetime.timedelta(hours=1)


        return a.T
    
    def __init__(self, *args, **kwargs):
        
        self.nameJsonFile = args[0]
        
        with open(self.nameJsonFile) as json_file:
            self.data = json.load(json_file)



class ReadWindCDMXJson(object):

    def _data(self):
        return self.data

    
    def _getNPWindbyD(self,date,station,hrs = 24):

        a = np.empty(shape=(hrs, 2), dtype=float)
        for i in range(hrs):
            if(i == 23):                
                a[i] =  [self.data["pollutionMeasurements"]["date"][date.strftime("%Y-%m-%d 24:00")]["WDR"][station], self.data["pollutionMeasurements"]["date"][date.strftime("%Y-%m-%d 24:00")]["WSP"][station]]
            else:
                a[i] =  [self.data["pollutionMeasurements"]["date"][date.strftime("%Y-%m-%d %H:%M")]["WDR"][station], self.data["pollutionMeasurements"]["date"][date.strftime("%Y-%m-%d %H:%M")]["WSP"][station]]

            date += datetime.timedelta(hours=1)

        return a.T
    

    def __init__(self, *args, **kwargs):
        
        self.nameJsonFile = args[0]
        
        with open(self.nameJsonFile) as json_file:
            self.data = json.load(json_file)

            
