import dash
import pathlib
import dash_core_components as dcc
import dash_html_components as html
import plotly.figure_factory as ff
import numpy as np
import matplotlib.pyplot as plt
from dash.dependencies import Input, Output
from numpy import random, exp

import sys
sys.path.append('../Whabha')
from Whabha import Whabha  



def f1(x,y):
    u = -2*x*exp(-y**2-x**2)
    v = -2*y*exp(-y**2-x**2)
    return u,v

def f2(x,y):
    u =  2*y*exp(-y**2-x**2)
    v = -2*x*exp(-y**2-x**2)
    return u,v

def f(x,y,c):
    uf1,vf1 = f1(x,y)
    uf2,vf2 = f2(x,y)

    u = 1.0/(1.0+c)*(uf1 + c*uf2);
    v = 1.0/(1.0+c)*(vf1 + c*vf2);
    return u,v


def graph_example(c,smoothp,n,m,ne,me):

    a = -2
    b =  2
    #c = 1.0
    x = a + (b-a)*random.rand(n,)
    y = a + (b-a)*random.rand(m,)


    u,v  = f(x,y,c)
    uv   = np.vstack([u, v]).T #= [u,v]

    # mpl_fig = plt.figure()
    # plt.quiver(x,y,u,v,color='blue')
    # plt.show()


    whabhaApprox = Whabha(x, y, uv, smooth=smoothp,mode='N-D')

    xe = np.linspace(a, b, ne)
    ye = np.linspace(a, b, me)
    xxe, yye = np.meshgrid(xe, ye)

    uve = whabhaApprox(xxe, yye)
    
    ue = uve[:,:,0];
    ve = uve[:,:,1];


    fig = ff.create_quiver(xxe, yye, ue, ve)




    fig.update_layout(
        autosize=False,
        width=500,
        height=500,
        margin=dict(
            l=50,
            r=50,
            b=50,
            t=50,
            pad=4
        ),
        paper_bgcolor="LightSteelBlue"
    )

    #print('nueva figura')
    return fig

external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
app = dash.Dash(__name__, external_stylesheets=external_stylesheets)

app.layout = html.Div([
    
    dcc.Graph(id='graph-with-slider'),

    dcc.Slider(
        id='smoothing-slider',
        min=0,
        max=1,
        step=0.1,        
        marks={i / 5: str(i / 5) for i in range(0, 5)},
        value=0.0001,        
    )
])



@app.callback(
    Output('graph-with-slider', 'figure'),
    Input('smoothing-slider', 'value'))
def update_figure(slider_value):

    figure = graph_example(c=1.0,smoothp=slider_value,n=10,m=10,ne=10,me=10)

    figure.update_layout(transition_duration=5)
    return  figure



if __name__ == '__main__':
    app.run_server(debug=True)
