import dash
import pathlib
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import plotly.graph_objs as go
from dash.dependencies import Input, Output
from plotly import tools


import plotly.figure_factory as ff
import numpy as np
from numpy import random, exp
import matplotlib.pyplot as plt

import sys
sys.path.append('../Whabha')
from Whabha import Whabha  


import re



# app = dash.Dash(
#     __name__, meta_tags=[{"name": "viewport", "content": "width=device-width"}]
# )




# server = app.server
# demo_mode = True


app = dash.Dash(__name__)
mathjax = 'https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-MML-AM_CHTML'
app.scripts.append_script({ 'external_url' : mathjax })

app.layout = html.Div(id='main',children=[
  html.P(children='Delicious \(\pi\) is inline with my goals.'),
  html.P(children='$$ \lim_{t \\rightarrow \infty} \pi = 0$$'),
  html.P(style={'text-align':'left'}, children='\(\\leftarrow \pi\)'),
  html.P(style={'text-align':'left'}, children='not much left.'),
  html.P(style={'text-align':'right'},children='\(\pi \\rightarrow\)'),
  html.P(style={'text-align':'right'},children='but it feels so right.'),
])

if __name__ == '__main__':
  app.run_server(debug=True)
