import dash
import pathlib
import dash_core_components as dcc
import dash_html_components as html
import pandas as pd
import plotly.graph_objs as go

from dash.dependencies import Input, Output
from plotly import tools


import plotly.figure_factory as ff
import numpy as np
import math
from numpy import random, exp
import matplotlib.pyplot as plt

import sys
sys.path.append('../Whabha')
sys.path.append('../Bembourhim')

from Whabha import Whabha  
from Bembourhim import Bembourhim  

import re

import dash_html_components as html
import dash_gif_component as gif

app = dash.Dash(
    __name__, meta_tags=[{"name": "viewport", "content": "width=device-width"}]
)


server = app.server
demo_mode = True


fig = go.Figure() # or any Plotly Express function e.g. px.bar(...)
# fig.add_trace( ... )
# fig.update_layout( ... )

def f1(x,y):
    u = -2*x*exp(-y**2-x**2)
    v = -2*y*exp(-y**2-x**2)
    return u,v

def f2(x,y):
    u =  2*y*exp(-y**2-x**2)
    v = -2*x*exp(-y**2-x**2)
    return u,v

def f(x,y,c):
    uf1,vf1 = f1(x,y)
    uf2,vf2 = f2(x,y)

    u = 1.0/(1.0+c)*(uf1 + c*uf2);
    v = 1.0/(1.0+c)*(vf1 + c*vf2);
    return u,v



def approx_graph(c,smoothp,rhop,n,m,ne,me,knots,approach):

    a = -2
    b =  2
    #c = 1.0

    wrerror = 0.0
    brerror = 0.0
    
    slapproach   = len(approach)
    
    if knots == "random":
        x = a + (b-a)*random.rand(n,)
        y = a + (b-a)*random.rand(m,)
        u,v  = f(x,y,c)
        uvt  = np.vstack([u,v]).T
        uv   = [u,v]

        fig2 = ff.create_quiver(x, y, u, v,name='Knots')
        
        if slapproach == 1:
            if approach[0] == "whabha":
                whabhaApprox = Whabha(x, y, uvt, smooth=smoothp,mode='N-D')

            if approach[0] == "bemborhim":
                bemApprox    = Bembourhim(x, y, uv, rho=rhop)

        if slapproach == 2:
            whabhaApprox = Whabha(x, y, uvt, smooth=smoothp,mode='N-D')
            bemApprox    = Bembourhim(x, y, uv, rho=rhop)
            
    else:    
        x = np.linspace(a, b, n)
        y = np.linspace(a, b, m)
        xx, yy = np.meshgrid(x,y)
        u,v  = f(xx,yy,c)
        uft = np.asarray(u.flatten())
        vft = np.asarray(v.flatten())   
        uvt = np.vstack([uft, vft]).T 
        uv  = [uft.T,vft.T]

        fig2 = ff.create_quiver(xx, yy, u, v,name='Knots')

        if slapproach == 1:
            if approach[0] == "whabha":
                whabhaApprox = Whabha(xx, yy, uvt, smooth=smoothp,mode='N-D')

            if approach[0] == "bemborhim":
                bemApprox    = Bembourhim(xx, yy, uv, rho=rhop)

        if slapproach == 2:
            whabhaApprox = Whabha(xx, yy, uvt, smooth=smoothp,mode='N-D')
            bemApprox    = Bembourhim(xx, yy, uv, rho=rhop)


    
    xe = np.linspace(a, b, ne)
    ye = np.linspace(a, b, me)
    xxe, yye = np.meshgrid(xe, ye)
    uue, vve = f(xxe,yye,c)
    
    if slapproach == 1:
        if approach[0] == "whabha":
            uvew = whabhaApprox(xxe, yye)
            uew  = uvew[:,:,0];
            vew  = uvew[:,:,1];
            #whabhaApprox = Whabha(xx, yy, uvt, smooth=smoothp,mode='N-D')

        if approach[0] == "bemborhim":
            ueb,veb = bemApprox(xxe, yye)
            #bemApprox    = Bembourhim(xx, yy, uv, rho=rhop)


    if slapproach == 2:
        uvew = whabhaApprox(xxe, yye)
        uew  = uvew[:,:,0];
        vew  = uvew[:,:,1];
        ueb,veb = bemApprox(xxe, yye)


    if slapproach == 0 or slapproach == 3:
        fig = fig2


        
    if slapproach == 1:
        if approach[0] == "whabha":
            fig  = ff.create_quiver(xxe, yye, uew, vew, name = 'Whabha_____')
        if approach[0] == "bemborhim":
            fig  = ff.create_quiver(xxe, yye, ueb, veb, name = 'Bembourhim')
            
        fig.add_traces(data = fig2.data)
            
    if slapproach == 2:
        fig3  = ff.create_quiver(xxe, yye, uew, vew, name = 'Whabha_____')
        fig  = ff.create_quiver(xxe, yye, ueb, veb, name = 'Bembourhim')
        fig.add_traces(data = fig2.data)
        fig.add_traces(data = fig3.data)
        

    if slapproach == 1:
        if approach[0] == "whabha":
            uw = np.array([uew.flatten(), vew.flatten()])
            ue = np.array([uue.flatten(), vve.flatten()])
            wrerror = np.linalg.norm(uw-ue,2)/np.linalg.norm(ue,2)

            
    if slapproach == 1:
        if approach[0] == "bemborhim":
            ub = np.array([ueb.flatten(), veb.flatten()])
            ue = np.array([uue.flatten(), vve.flatten()])
            brerror = np.linalg.norm(ub-ue,2)/np.linalg.norm(ue,2)

    
    if slapproach == 2:    
        uw = np.array([uew.flatten(), vew.flatten()])
        ub = np.array([ueb.flatten(), veb.flatten()])
        ue = np.array([uue.flatten(), vve.flatten()])
        wrerror = np.linalg.norm(uw-ue,2)/np.linalg.norm(ue,2)
        brerror = np.linalg.norm(ub-ue,2)/np.linalg.norm(ue,2)
        
        
    
    fig.update_layout(
        autosize=False,
        width=1000,
        height=1000,
        margin=dict(
            l=50,
            r=50,
            b=50,
            t=50,
            pad=4
        ),
        paper_bgcolor="LightSteelBlue"
    )

    return wrerror,brerror,fig




def graph():

    return html.Div(
        className = "row",
        children=[

            html.Div(
                className="two columns",
                style={"padding-bottom": "5%"},
                #style={"padding": "0px 10px"},

                children=[

                    html.Div(
                        [
                            
                            html.P(
                                "C parameter:",
                                style={"font-weight": "bold", "margin-bottom": "0px"},
                                className="plot-display-text",
                            ),
                            
                        ],
                        className="entropy-div",                        
                    ),
                    

                    
                    html.Div(
                        [
                            
                            dcc.Input(id="input_range2", value=1, type="number", min=0, max=1000, style={'width':'400%'},),
                            #dcc.Input(id="input_range3", value=10, type="number", placeholder="Mx", debounce=True, min=0, max=1000, step=.00001,style={'width':'400%'},),
                            
                        ],
                        style={'width' : '8%', 'marginLeft': 0 , 'marginTop' : 0 ,'minWidth' : 10  },
                    ),                            
                    
                    html.Hr(),
                    
                    html.Div(
                        [
                            
                            html.P(
                                "Knots :",
                                style={"font-weight": "bold", "margin-bottom": "0px"},
                                className="plot-display-text",
                            ),
                            
                            
                            html.Div(
                                [
                                    dcc.RadioItems(
                                        options=[
                                            {
                                                "label": "Uniform",
                                                "value": "uniform",
                                            },
                                            {
                                                "label": "Random",
                                                "value": "random",
                                            },
                                        ],
                                        value="uniform",
                                        id=f"radio_knots",
                                        labelStyle={"verticalAlign": "middle"},
                                        className="plot-display-radio-items",
                                    )
                                ],
                                className="radio-item",
                            ),
                            


                            
                            html.Div(
                                [
                                    dcc.Input(id="input_nknots", value=50.0, type="number", min=20, max=10000,style={'width':'100%'},),
                                ],
                                style={'width' : '50%', 'marginLeft': 0 , 'marginTop' : 0 ,'minWidth' : 10  },
                            ),                                                        
                        ],
                        
                    ),                            

                    html.Hr(),
                    
                    html.Div(
                        [
                            #html.Hr(),
                            
                            html.P(
                                "Approaches :",
                                style={"font-weight": "bold", "margin-bottom": "0px"},
                                className="plot-display-text",
                            ),
                              
                            html.Div(
                                [
                                    dcc.Checklist(
                                        options=[
                                            {
                                                "label": "Whabha ",
                                                "value": "whabha",
                                            },
                                            {
                                                "label": "Bembourhim",
                                                "value": "bemborhim",
                                            },
                                            {
                                                "label": "Div-Free-Approx",
                                                "value": "divfreeapprox",
                                            },
                                        ],
                                        value=["whabha"],
                                        id=f"radio_approach",
                                        labelStyle={"verticalAlign": "middle"},
                                        className="plot-display-radio-items",
                                    )
                                ],
                                className="radio-item-div",
                            ),

                            html.Hr(),
                            html.Div(
                                [
                                    
                                    html.P(
                                        "Smoothing:",
                                        style={"font-weight": "bold", "margin-bottom": "0px"},
                                        className="plot-display-text",
                                    ),
                                    
                                ],
                                className="entropy-div2",                        
                            ),
                    


                            
                            html.Div(
                                [
                                    dcc.Input(id="input_range3", value=0.0001, type="number",min=0, max=1),
                                ],
                                style={'width' : '50%', 'marginLeft': 0 , 'marginTop' : 0 ,'minWidth' : 10  },
                            ),                                                        
                        ],
                        
                    ),                            


                    




                    html.Div(
                        [                            
                            html.Div(
                                [
                                    
                                    html.P(
                                        "Rho :",
                                        style={"font-weight": "bold", "margin-bottom": "0px"},
                                        className="plot-display-text",
                                    ),
                                    
                                ],
                                className="entropy-div3",                        
                            ),
                            
                            html.Div(
                                [
                                    dcc.Input(id="input_range4", value=50, type="number", min=0, max=1000,),
                                ],
                                style={'width' : '50%', 'marginLeft': 0 , 'marginTop' : 0 ,'minWidth' : 10  },
                            ),                                                        
                        ],                                                
                    ),                            
                    
                    
                    html.Hr(),
                    
                    
                    html.Div(
                        [
                                    
                            html.P(                                        
                                "Relative Error :",
                                style={"font-weight": "bold", "margin-bottom": "0px"},
                                className="plot-display-text",
                            ),
                        ]),                            
                    html.Div(
                        [
                            html.Div(id='my-output1'),
                            html.Div(id='my-output2'),
                        ]),
                    
                    
                    
                    
                    
                    # html.Div(
                    #     [
                    
                    #         html.P(                                        
                    #             "RSME :",
                    #             html.Div(id='my-output'),
                    #             style={"font-weight": "bold", "margin-bottom": "0px"},
                    #             className="plot-display-text",
                    #         ),
                    
                    #     ],
                    #     className="entropy-div3",                        
                    # ),
                    
                    
                    # html.Div(
                    #     [
                    
                    #         html.Div("Approx error: {error}.4f"),
                    #     ],
                    #     #style={'width' : '50%', 'marginLeft': 0 , 'marginTop' : 0 ,'minWidth' : 10  },
                    # ),                                                        
                    
                    
                    # ],                                                
                    #     ),                            
                    
                ],
                
            ),
            
            html.Div(
                style={"padding": "10px 250px"},
                
                children = [dcc.Graph(id="graphica")]
            ),                    



        ],
    )





Markdown_text = '''

#####
------------

##### In this example, we consider the vector field &#402;&#99;&#58;&#8477;&sup2;&rarr;&#8477;&sup2; where  &#402;&#99;&#40;&#120;&#44;&#121;&#41; &#61; &#40;&#402;&#49;&#40;&#120;&#44; &#121;&#41;&#43;&#99;&#402;&#50;&#40;&#120;&#44; &#121;&#41;&#41;&#47;&#40;&#49; &#43; &#99; &#41;, with  &#99; a positive parameter. 

##### The functions &#402;&#49; and &#402;&#50; are curl-free and div-free vector fields given in terms of the function &#402;&#40;&#120;&#44;&#121;&#41;&#61;&#101;&#120;&#112;&#40;&ndash;&#120;&sup2;&minus;&#121;&sup2;&#41; by &#402;&#49;&#61;&#40;&#8706;&#8321;&#402;&#44;&#8706;&#8322; &#402;&#41;&rsquo;
##### and  &#402;&#50; &#61; &#40;&ndash;&#8706;&#8322;&#402;&#44;&#8706;&#8321;&#402;&#41;&rsquo; respectly.








'''

#Markdown_text = convert(Markdown_text)




app.layout = html.Div(
    style={"height": "100%"},

    children=[
        
        # Banner display
        html.Div(
            [
                html.H2(
                    "Vector Field Approximation",
                    id="title",
                    className="eight columns",
                    style={"margin-left": "3%"},
                ),
            ],
            className="banner row",
        ),
        

        html.Div([
            
            dcc.Markdown(Markdown_text, dangerously_allow_html=True),                

        ]),


        html.Div(className="container", children=[graph()]),
        
        html.Div(
            style={"padding": "10px 250px"},                
            children=[
                gif.GifPlayer(
                    gif='assets/o3_mygif.gif',
                    still='assets/dash-logo.png',
                    autoplay = True
                    #         # alt='adorable puppy',
                )
            ]
        ),                    

        html.Div(
            style={"padding": "10px 250px"},                
            children=[
                gif.GifPlayer(
                    gif='assets/pm10_mygif.gif',
                    still='assets/dash-logo.png',
                    autoplay = True
                    #         # alt='adorable puppy',
                )
            ]
        ),                    
        
        html.Div(
            style={"padding": "10px 250px"},                
            children=[
                gif.GifPlayer(
                    gif='assets/so2_mygif.gif',
                    still='assets/dash-logo.png',
                    autoplay = True
                    #         # alt='adorable puppy',
                )
            ]
        ),                    


        
        
    ]
)




@app.callback(
    Output("my-output1", component_property='children'),
    Output("my-output2", component_property='children'),
    Output("graphica","figure"),    
    Input("input_range2","value"),
    Input("input_range3","value"),
    Input("input_range4","value"),
    Input("radio_knots", "value"),
    Input("input_nknots","value"),
    Input("radio_approach","value"),    
)
def update_graph_callback(input_c,input_smooth,input_rho,
                           input_knots,input_nknots,
                           radio_approach):
    
    if input_smooth is None:
        input_smooth = 0.0

    if input_c is None:
        input_c = 0.0
     
    if input_rho is None:
        input_smooth = 1.0
 
    if input_nknots is None:
        input_nknots = 50
        
    if input_knots == "uniform":                
        nn = int(math.sqrt(input_nknots))
        mm = nn
    else:
        nn = input_nknots
        mm = input_nknots
    
    
    wrerror, brerror, figure = approx_graph(c=input_c,smoothp=input_smooth,rhop=input_rho,n=nn,m=mm,ne=20,me=20,knots=input_knots,approach=radio_approach)            
    return 'Whabha: {}'.format(wrerror), 'Bembourhim: {}'.format(brerror),figure




# @app.callback(
#     Output(component_id='my-output', component_property='children'),
#     Input(component_id='my-input', component_property='value')
# )
# def update_output_div(input_value):
#     return 'Output: {}'.format(input_value)


# @app.callback([Output('confirmed', 'children'), Output('recovered', 'children'),
#                Output('deaths', 'children')], [Input('countries', 'value')])
# def update_summary(country):
    
#     if country == 'country_1':
#         return [1000, 2000, 3000]

#     else:
#         return [4000, 5000, 6000]


# Running the server
if __name__ == "__main__":
    app.run_server(debug=True)

