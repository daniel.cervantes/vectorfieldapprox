from mpl_toolkits.basemap import Basemap
import matplotlib.pyplot as plt

map = Basemap(llcrnrlon=-117.132888,llcrnrlat=13.498459,urcrnrlon=-82.340463,urcrnrlat=32.681497,resolution='i')

map.drawmapboundary(fill_color='aqua')
map.fillcontinents(color='#ddaa66',lake_color='aqua')
map.drawcoastlines()

map.readshapefile('Mexico_Ciudades', 'Mexico_Ciudades')

plt.show()
