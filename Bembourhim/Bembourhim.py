
import numpy as np
import math 
from scipy import linalg
from scipy.special import xlogy
from scipy.spatial.distance import cdist, pdist, squareform


class Bembourhim(object):

    def _h_d2xxK3(self,x,y,xc,yc):
        return 5*((y-yc)**2+(x-xc)**2)**(3/2) + 15*(x-xc)**2*math.sqrt((y-yc)**2+(x-xc)**2);
    
    def _h_d2xyK3(self,x,y,xc,yc):
        return 15*(x-xc)*math.sqrt((y-yc)**2+(x-xc)**2)*(y-yc);

    def _h_d2yxK3(self,x,y,xc,yc):
        return 15*(x-xc)*math.sqrt((y-yc)**2+(x-xc)**2)*(y-yc);

    def _h_d2yyK3(self,x,y,xc,yc):
        return 15*(y-yc)**2*math.sqrt((y-yc)**2+(x-xc)**2) + 5*((y-yc)**2+(x-xc)**2)**(3/2);

    def _h_lK3(self,x,y,xc,yc):
        return 15*math.sqrt((y-yc)**2+(x-xc)**2)*(y-yc)**2+10*((y-yc)**2+(x-xc)**2)**(3/2)+15*(x-xc)**2*math.sqrt((y-yc)**2+(x-xc)**2);

    def __init__(self, *args, **kwargs):

        self.rho  = kwargs.pop('rho',1.0)
        self.xi   = np.asarray([np.asarray(a,dtype=np.float_).flatten() for a in args[:-1]])
        self.N    = self.xi.shape[-1]
        self.di   = np.asarray([np.asarray(a,dtype=np.float_).flatten() for a in args[-1:]])
        z6x1      = np.zeros((6,1))
        self.di   = np.concatenate((self.di.T,z6x1))

        self.nodes = linalg.solve(self.A,self.di)

        #print(self.A)
        #np.savetxt('Gram.txt',self.A,fmt='%.3f')
        #print(self.di)
        #print(self.nodes)
        #print(self.nodes.shape)
        # print(self.xi)
        # print(self.di)

    @property
    def A(self):

        G    = np.zeros((2*self.N+6, 2*self.N+6))
        P    = np.zeros((2*self.N,6))
        Z6x6 = np.zeros((6,6))

      
        for i in range(0,self.N):
            for j in range(0,self.N):
                x  = self.xi[0,i]
                y  = self.xi[1,i]
                xc = self.xi[0,j]
                yc = self.xi[1,j]
                
                G[i,j] =                 -self._h_lK3(x,y,xc,yc) + (1.0-1.0/self.rho)*self._h_d2xxK3(x,y,xc,yc)                
                G[i,j+self.N] =                                    (1.0-1.0/self.rho)*self._h_d2yxK3(x,y,xc,yc)                
                G[i+self.N,j] =                                    (1.0-1.0/self.rho)*self._h_d2xyK3(x,y,xc,yc)
                G[i+self.N,j+self.N] =   -self._h_lK3(x,y,xc,yc) + (1.0-1.0/self.rho)*self._h_d2yyK3(x,y,xc,yc)
                
        for i in range(0,self.N):
            P[i,0]        = 1;
            P[i,1]        = self.xi[0,i];
            P[i,2]        = self.xi[1,i];

            P[i+self.N,3] = 1.0;
            P[i+self.N,4] = self.xi[0,i];
            P[i+self.N,5] = self.xi[1,i];


        G[0:2*self.N,2*self.N:2*self.N+6] = P
        G[2*self.N:2*self.N+6,0:2*self.N] = P.T

        # np.savetxt('Gram.txt',G,fmt='%.3f')
        
        return G 

    def __call__(self,*args):
        
        args   = [np.asarray(x) for x in args]    
        Ne     = args[0].size

        xa = np.asarray([a.flatten() for a in args], dtype=np.float_)

        # print(Ne)
        # print(xa)
        # print(self.xi)
            
        Ge   = np.zeros((2*Ne,2*self.N+6))
        P    = np.zeros((2*Ne,6))


        for i in range(0,Ne):
            for k in range(0,self.N):
                
                x  = xa[0,i]
                y  = xa[1,i]            
                xc = self.xi[0,k]
                yc = self.xi[1,k]
                
                # print(x,y,xc,yc)
                
                Ge[i,k] =                 -self._h_lK3(x,y,xc,yc) + (1.0-1.0/self.rho)*self._h_d2xxK3(x,y,xc,yc)                
                Ge[i,k+self.N] =                                    (1.0-1.0/self.rho)*self._h_d2yxK3(x,y,xc,yc)                
                Ge[i+Ne,k] =                                        (1.0-1.0/self.rho)*self._h_d2xyK3(x,y,xc,yc)
                Ge[i+Ne,k+self.N] =       -self._h_lK3(x,y,xc,yc) + (1.0-1.0/self.rho)*self._h_d2yyK3(x,y,xc,yc)

                    
                
        for i in range(0,Ne):
            P[i,0]    = 1.0;
            P[i,1]    = xa[0,i];
            P[i,2]    = xa[1,i];
            
            P[i+Ne,3] = 1.0;
            P[i+Ne,4] = xa[0,i];
            P[i+Ne,5] = xa[1,i];

            
        Ge[0:2*Ne,2*self.N:2*self.N+6] = P

        # np.savetxt('GramE.txt',Ge,fmt='%.3f')
        
        uv = np.dot(Ge,self.nodes)
        u  = uv[0:Ne];
        v  = uv[Ne:2*Ne];
        return u.reshape(args[0].shape), v.reshape(args[0].shape) 
    
